use std::env;
use std::fs;

fn parse_file(contents: &str) -> Vec<((i32, i32), (i32, i32))> {
    let mut lines: Vec<((i32, i32), (i32, i32))> = Vec::new();
    for line in contents.lines() {
        let split_line: Vec<&str> = line.split(" -> ").collect();
        let (point1, point2): (Vec<&str>, Vec<&str>) = (
            split_line[0].split(",").collect(),
            split_line[1].split(",").collect(),
        );
        let (x1, y1) = (
            point1[0].parse::<i32>().unwrap(),
            point1[1].parse::<i32>().unwrap(),
        );
        let (x2, y2) = (
            point2[0].parse::<i32>().unwrap(),
            point2[1].parse::<i32>().unwrap(),
        );
        lines.push(((x1, y1), (x2, y2)));
    }

    lines
}

fn part1(lines: Vec<((i32, i32), (i32, i32))>) {
    let x_values = lines
        .iter()
        .map(|((x1, _), (x2, _))| vec![*x1, *x2])
        .flatten();
    let y_values = lines
        .iter()
        .map(|((_, y1), (_, y2))| vec![*y1, *y2])
        .flatten();

    let max_y = y_values.max().unwrap();
    let max_x = x_values.max().unwrap();

    let mut grid: Vec<Vec<i32>> = (0..max_y + 1)
        .map(|_| (0..max_x + 1).map(|_| 0).collect())
        .collect();

    for ((x1, y1), (x2, y2)) in lines.iter() {
        if x1 == x2 || y1 == y2 {
            let (&min_x, &max_x) = (std::cmp::min(x1, x2), std::cmp::max(x1, x2));
            let (&min_y, &max_y) = (std::cmp::min(y1, y2), std::cmp::max(y1, y2));
            for x in min_x..max_x + 1 {
                for y in min_y..max_y + 1 {
                    grid[y as usize][x as usize] += 1;
                }
            }
        }
    }

    let total: i32 = grid.into_iter().flatten().filter(|point| *point >= 2).sum();
    println!("{}", total);
}

fn part2(lines: Vec<((i32, i32), (i32, i32))>) {
    let x_values = lines
        .iter()
        .map(|((x1, _), (x2, _))| vec![*x1, *x2])
        .flatten();
    let y_values = lines
        .iter()
        .map(|((_, y1), (_, y2))| vec![*y1, *y2])
        .flatten();

    let max_y = y_values.max().unwrap();
    let max_x = x_values.max().unwrap();

    let mut grid: Vec<Vec<i32>> = (0..max_y + 1)
        .map(|_| (0..max_x + 1).map(|_| 0).collect())
        .collect();

    for ((x1, y1), (x2, y2)) in lines.iter() {
        let (&min_x, &max_x) = (std::cmp::min(x1, x2), std::cmp::max(x1, x2));
        if x1 == x2 || y1 == y2 {
            let (&min_y, &max_y) = (std::cmp::min(y1, y2), std::cmp::max(y1, y2));
            for x in min_x..max_x + 1 {
                for y in min_y..max_y + 1 {
                    grid[y as usize][x as usize] += 1;
                }
            }
        } else {
            let slope = (y2 - y1) / (x2 - x1);
            let intercept = y1 - (slope * x1);
            for x in min_x..max_x + 1 {
                let y = (slope * x) + intercept;
                grid[y as usize][x as usize] += 1;
            }
        }
    }

    let total: i32 = grid.into_iter().flatten().filter(|point| *point >= 2).sum();
    println!("{}", total);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    println!("input file {}", filename);
    let contents = fs::read_to_string(filename).expect("failed to read file");
    let lines = parse_file(&contents);
    part1(lines.to_vec());
    part2(lines.to_vec());
}
