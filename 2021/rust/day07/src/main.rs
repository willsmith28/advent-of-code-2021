use std::env;
use std::fs;

fn part1(input_data: &str) {
    let positions: Vec<i32> = input_data
        .split(",")
        .map(|item| item.parse::<i32>().unwrap())
        .collect();

    let mut lowest_fuel_spent = i32::MAX;
    for position_index in 0..*positions.iter().max().unwrap() {
        let total_fuel: i32 = positions
            .iter()
            .map(|position| (position - position_index).abs())
            .sum();
        if total_fuel < lowest_fuel_spent {
            lowest_fuel_spent = total_fuel;
        }
    }

    println!("{}", lowest_fuel_spent);
}

fn part2(input_data: &str) {
    let positions: Vec<i32> = input_data
        .split(",")
        .map(|item| item.parse::<i32>().unwrap())
        .collect();
    let mut lowest_fuel_spent = i32::MAX;

    for position_index in 0..*positions.iter().max().unwrap() {
        let total_fuel: i32 = positions
            .iter()
            .map(|position| {
                let n = (position - position_index).abs();
                (n * (n + 1)) / 2
            })
            .sum();
        if total_fuel < lowest_fuel_spent {
            lowest_fuel_spent = total_fuel;
        }
    }
    println!("{}", lowest_fuel_spent);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    println!("input file {}", filename);
    let contents = fs::read_to_string(filename).expect("failed to read file");
    let input_data = contents.lines().next().unwrap();
    part1(input_data);
    part2(input_data);
}
