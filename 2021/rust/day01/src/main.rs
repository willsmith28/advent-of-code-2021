use std::env;
use std::fs;

fn part1(contents: &str) {
    let mut total: i32 = 0;
    let mut prev_item: Option<i32> = None;
    for line in contents.lines() {
        let current_value = match line.parse::<i32>() {
            Ok(val) => val,
            Err(_) => break,
        };
        match prev_item {
            Some(prev_value) => {
                if prev_value < current_value {
                    total += 1;
                }
            }
            None => (),
        }
        prev_item = Some(current_value)
    }

    println!(
        "{}  measurements are larger than the previous measurement",
        total
    )
}

fn part2(contents: &str, window_size: i32) {
    // let summed_values = Vec::new();
    let input_data: Vec<i32> = contents
        .lines()
        .map(|item| item.parse::<i32>().unwrap())
        .collect();

    let mut summed_values: Vec<i32> = Vec::new();
    for i in 0..(input_data.len() as i32 - window_size + 1) {
        let mut current_sum = 0;
        for j in 0..window_size {
            current_sum += input_data[(i + j) as usize];
        }
        summed_values.push(current_sum);
    }

    let mut total = 0;
    let mut prev_item: Option<i32> = None;
    for current_value in summed_values.iter() {
        match prev_item {
            Some(prev_value) => {
                if prev_value < *current_value {
                    total += 1;
                }
            }
            None => (),
        }
        prev_item = Some(*current_value);
    }

    println!("{} sums are larger than the previous sum", total)
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    println!("input file {}", filename);

    let contents = fs::read_to_string(filename).expect("failed to read file");
    part1(&contents);
    part2(&contents, 3)
}
