use std::env;
use std::fs;

fn part1(contents: &str) {
    let mut horizontal_position = 0;
    let mut depth = 0;

    for line in contents.lines() {
        let value = line.split_whitespace().take(2).collect::<Vec<&str>>();
        match value.as_slice() {
            ["forward", value] => horizontal_position += value.parse::<i32>().unwrap(),
            ["up", value] => depth -= value.parse::<i32>().unwrap(),
            ["down", value] => depth += value.parse::<i32>().unwrap(),
            [..] => (),
        }
    }
    println!(
        "{} is the result of multiplying the final horizontal position by the final depth",
        depth * horizontal_position
    )
}

fn part2(contents: &str) {
    let mut horizontal_position = 0;
    let mut depth = 0;
    let mut aim = 0;

    for line in contents.lines() {
        let value = line.split_whitespace().take(2).collect::<Vec<&str>>();
        match value.as_slice() {
            ["forward", value] => {
                let v = value.parse::<i32>().unwrap();
                horizontal_position += v;
                depth += aim * v;
            }
            ["up", value] => aim -= value.parse::<i32>().unwrap(),
            ["down", value] => aim += value.parse::<i32>().unwrap(),
            [..] => (),
        }
    }

    println!(
        "{} is the result of multiplying the final horizontal position by the final depth",
        depth * horizontal_position
    )
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    println!("input file {}", filename);
    let contents = fs::read_to_string(filename).expect("failed to read file");

    part1(&contents);
    part2(&contents);
}
