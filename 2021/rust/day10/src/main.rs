use std::collections::hash_map;
use std::collections::hash_set;
use std::env;
use std::fs;

enum ParseResult {
    IllegalChar(char),
    UnclosedOpenings(Vec<char>),
}

fn parse_line(
    line: &str,
    close_open_mapping: &hash_map::HashMap<char, char>,
    open_close_mapping: &hash_map::HashMap<char, char>,
) -> ParseResult {
    let mut stack = vec![];
    for character in line.chars() {
        if open_close_mapping.contains_key(&character) {
            stack.push(character)
        } else if close_open_mapping.contains_key(&character) {
            let popped_char = stack.pop().unwrap();
            if popped_char != *close_open_mapping.get(&character).unwrap() {
                return ParseResult::IllegalChar(character);
            }
        }
    }
    ParseResult::UnclosedOpenings(stack)
}

fn part1(
    input_data: &Vec<&str>,
    close_open_mapping: &hash_map::HashMap<char, char>,
    open_close_mapping: &hash_map::HashMap<char, char>,
) -> hash_set::HashSet<usize> {
    let mut illegal_chars = vec![];
    let mut currupt_indexes: hash_set::HashSet<usize> = hash_set::HashSet::new();

    for (index, line) in input_data.iter().enumerate() {
        let parse_result = parse_line(line, close_open_mapping, open_close_mapping);
        match parse_result {
            ParseResult::IllegalChar(c) => {
                currupt_indexes.insert(index);
                illegal_chars.push(c);
            }
            ParseResult::UnclosedOpenings(_) => (),
        }
    }

    let score_mapping = hash_map::HashMap::from([(')', 3), (']', 57), ('}', 1197), ('>', 25137)]);
    let total: i32 = illegal_chars
        .iter()
        .map(|character| score_mapping.get(&character).unwrap())
        .sum();
    println!("{}", total);
    currupt_indexes
}

fn part2(
    incomplete_lines: &Vec<&str>,
    close_open_mapping: &hash_map::HashMap<char, char>,
    open_close_mapping: &hash_map::HashMap<char, char>,
) {
    let score_mapping = hash_map::HashMap::from([(')', 1), (']', 2), ('}', 3), ('>', 4)]);
    let mut scores: Vec<isize> = incomplete_lines
        .iter()
        .map(|line| {
            let mut total_score = 0;
            match parse_line(line, close_open_mapping, open_close_mapping) {
                ParseResult::IllegalChar(_) => panic!(),
                ParseResult::UnclosedOpenings(unclosed_openings) => {
                    for opening_char in unclosed_openings.into_iter().rev() {
                        let closing_char = open_close_mapping.get(&opening_char).unwrap();
                        total_score *= 5;
                        total_score += score_mapping.get(closing_char).unwrap();
                    }
                }
            }
            total_score
        })
        .collect();

    scores.sort();
    println!("{}", scores[scores.len() / 2])
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    println!("input file {}", filename);
    let contents = fs::read_to_string(filename).expect("failed to read file");
    let input_data: Vec<&str> = contents.lines().collect();
    let close_open_mapping =
        hash_map::HashMap::from([('}', '{'), (')', '('), (']', '['), ('>', '<')]);
    let open_close_mapping =
        hash_map::HashMap::from([('{', '}'), ('(', ')'), ('[', ']'), ('<', '>')]);

    let currupt_indexes = part1(&input_data, &close_open_mapping, &open_close_mapping);
    let incomplete_lines: Vec<&str> = input_data
        .into_iter()
        .enumerate()
        .filter(|(index, _)| !currupt_indexes.contains(index))
        .map(|(_, line)| line)
        .collect();
    part2(&incomplete_lines, &close_open_mapping, &open_close_mapping)
}
