use std::env;
use std::fs;

fn part1(contents: &str) {
    let input_data: Vec<&str> = contents.lines().collect();

    let mut gamma_rate = String::new();
    let mut epsilon_rate = String::new();

    for index in 0..input_data[0].len() {
        // count number of 1s and 0s
        let mut number_of_1s = 0;
        let mut number_of_0s = 0;
        for item in input_data.iter() {
            let bit = item.chars().nth(index).unwrap();
            if bit == '1' {
                number_of_1s += 1;
            } else {
                number_of_0s += 1;
            }
        }

        if number_of_1s > number_of_0s {
            gamma_rate.push('1');
            epsilon_rate.push('0');
        } else {
            gamma_rate.push('0');
            epsilon_rate.push('1');
        }
    }

    let gamma_rate = isize::from_str_radix(&gamma_rate, 2).unwrap();
    let epsilon_rate = isize::from_str_radix(&epsilon_rate, 2).unwrap();

    println!("{}", gamma_rate * epsilon_rate)
}

fn part2(contents: &str) {
    let input_data: Vec<&str> = contents.lines().collect();
    let mut oxygen_rating = input_data.to_vec();
    let mut index = 0;
    while oxygen_rating.len() > 1 {
        let mut number_of_1s = 0;
        let mut number_of_0s = 0;
        for item in oxygen_rating.iter() {
            let bit = item.chars().nth(index).unwrap();
            if bit == '1' {
                number_of_1s += 1;
            } else {
                number_of_0s += 1;
            }
        }

        let most_common_bit = if number_of_1s >= number_of_0s {
            '1'
        } else {
            '0'
        };

        oxygen_rating.retain(|item| item.chars().nth(index).unwrap() == most_common_bit);
        index += 1;
    }

    let mut c02_rating = input_data.to_vec();
    index = 0;
    while c02_rating.len() > 1 {
        let mut number_of_1s = 0;
        let mut number_of_0s = 0;
        for item in c02_rating.iter() {
            let bit = item.chars().nth(index).unwrap();
            if bit == '1' {
                number_of_1s += 1;
            } else {
                number_of_0s += 1;
            }
        }

        let least_common_bit = if number_of_1s >= number_of_0s {
            '0'
        } else {
            '1'
        };

        c02_rating.retain(|item| item.chars().nth(index).unwrap() == least_common_bit);
        index += 1;
    }

    let oxygen_rating = isize::from_str_radix(oxygen_rating.iter().nth(0).unwrap(), 2).unwrap();
    let c02_rating = isize::from_str_radix(c02_rating.iter().nth(0).unwrap(), 2).unwrap();

    println!("{}", oxygen_rating * c02_rating);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    println!("input file {}", filename);
    let contents = fs::read_to_string(filename).expect("failed to read file");
    part1(&contents);
    part2(&contents);
}
