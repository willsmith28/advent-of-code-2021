use std::env;
use std::fs;

fn part1(input_data: &str, number_of_days: i32) {
    let mut lantern_fish: Vec<i32> = input_data
        .split(",")
        .map(|item| item.parse::<i32>().unwrap())
        .collect();

    for _ in 0..number_of_days {
        let mut new_fish = 0;
        for fish_index in 0..lantern_fish.len() {
            if lantern_fish[fish_index] == 0 {
                lantern_fish[fish_index] = 6;
                new_fish += 1;
            } else {
                lantern_fish[fish_index] -= 1;
            }
        }

        lantern_fish.extend((0..new_fish).map(|_| 8));
    }

    println!(
        "after {} days there are {} fish",
        number_of_days,
        lantern_fish.len()
    )
}

fn part2(input_data: &str, number_of_days: i32) {
    let input_data: Vec<i32> = input_data
        .split(",")
        .map(|item| item.parse::<i32>().unwrap())
        .collect();
    let mut lantern_fish: Vec<usize> = (0..9)
        .map(|index| input_data.iter().filter(|&item| *item == index).count())
        .collect();
    for _ in 0..number_of_days {
        let fish_at_zero = lantern_fish[0];
        lantern_fish.drain(0..1);
        lantern_fish[6] += fish_at_zero;
        lantern_fish.push(fish_at_zero);
    }
    let total: usize = lantern_fish.iter().sum();
    println!("after {} days there are {} fish", number_of_days, total)
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    println!("input file {}", filename);
    let contents = fs::read_to_string(filename).expect("failed to read file");
    let input_data = contents.lines().next().unwrap();
    part1(&input_data, 80);
    part2(&input_data, 256);
}
