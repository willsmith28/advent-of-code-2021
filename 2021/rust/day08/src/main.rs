use std::collections;
use std::env;
use std::fs;

fn part1(contents: &str) {
    let total: i32 = contents
        .lines()
        .map(|line| {
            let patterns_and_values = line.split("|");
            let output_values = patterns_and_values.last().unwrap();
            let mut line_total = 0;
            for value in output_values.split_whitespace() {
                let value_length = value.chars().count();
                if value_length == 2 || value_length == 3 || value_length == 4 || value_length == 7
                {
                    line_total += 1;
                }
            }
            line_total
        })
        .sum();
    println!("{}", total);
}

fn part2(contents: &str) {
    let total: i32 = contents
        .lines()
        .map(|line| {
            let patterns_and_values: Vec<&str> = line.split("|").collect();
            let patterns: Vec<collections::hash_set::HashSet<char>> = patterns_and_values[0]
                .split_whitespace()
                .map(|pattern| pattern.chars().collect())
                .collect();

            let output_values = patterns_and_values[1].split_whitespace().map(|pattern| {
                let mut pattern_chars: Vec<char> = pattern.chars().collect();
                pattern_chars.sort();
                pattern_chars.into_iter().collect::<String>()
            });

            let one = patterns
                .iter()
                .filter(|pattern| pattern.len() == 2)
                .next()
                .unwrap();
            let seven = patterns
                .iter()
                .filter(|pattern| pattern.len() == 3)
                .next()
                .unwrap();
            let four = patterns
                .iter()
                .filter(|pattern| pattern.len() == 4)
                .next()
                .unwrap();
            let eight = patterns
                .iter()
                .filter(|pattern| pattern.len() == 7)
                .next()
                .unwrap();

            let nine = patterns
                .iter()
                .filter(|pattern| {
                    pattern.len() == 6
                        && one.is_subset(pattern)
                        && seven.is_subset(pattern)
                        && four.is_subset(pattern)
                })
                .next()
                .unwrap();

            let zero = patterns
                .iter()
                .filter(|pattern| {
                    pattern.len() == 6
                        && *pattern != nine
                        && one.is_subset(pattern)
                        && seven.is_subset(pattern)
                })
                .next()
                .unwrap();

            let six = patterns
                .iter()
                .filter(|pattern| pattern.len() == 6 && *pattern != nine && *pattern != zero)
                .next()
                .unwrap();

            let three = patterns
                .iter()
                .filter(|pattern| {
                    pattern.len() == 5 && one.is_subset(pattern) && seven.is_subset(pattern)
                })
                .next()
                .unwrap();

            let five = patterns
                .iter()
                .filter(|pattern| {
                    pattern.len() == 5 && *pattern != three && nine.difference(pattern).count() == 1
                })
                .next()
                .unwrap();

            let two = patterns
                .iter()
                .filter(|pattern| pattern.len() == 5 && *pattern != three && *pattern != five)
                .next()
                .unwrap();

            let mut zero: Vec<&char> = zero.into_iter().collect();
            let mut one: Vec<&char> = one.into_iter().collect();
            let mut two: Vec<&char> = two.into_iter().collect();
            let mut three: Vec<&char> = three.into_iter().collect();
            let mut four: Vec<&char> = four.into_iter().collect();
            let mut five: Vec<&char> = five.into_iter().collect();
            let mut six: Vec<&char> = six.into_iter().collect();
            let mut seven: Vec<&char> = seven.into_iter().collect();
            let mut eight: Vec<&char> = eight.into_iter().collect();
            let mut nine: Vec<&char> = nine.into_iter().collect();
            zero.sort();
            one.sort();
            two.sort();
            three.sort();
            four.sort();
            five.sort();
            six.sort();
            seven.sort();
            eight.sort();
            nine.sort();

            let mut known_values = collections::hash_map::HashMap::new();
            known_values.insert(zero.into_iter().collect::<String>(), '0');
            known_values.insert(one.into_iter().collect::<String>(), '1');
            known_values.insert(two.into_iter().collect::<String>(), '2');
            known_values.insert(three.into_iter().collect::<String>(), '3');
            known_values.insert(four.into_iter().collect::<String>(), '4');
            known_values.insert(five.into_iter().collect::<String>(), '5');
            known_values.insert(six.into_iter().collect::<String>(), '6');
            known_values.insert(seven.into_iter().collect::<String>(), '7');
            known_values.insert(eight.into_iter().collect::<String>(), '8');
            known_values.insert(nine.into_iter().collect::<String>(), '9');

            output_values
                .map(|value| {
                    match known_values.get(&value) {
                        Some(number) => number,
                        None => panic!(),
                    }
                })
                .collect::<String>()
                .parse::<i32>()
                .unwrap()
        })
        .sum();

    println!("{}", total);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    println!("input file {}", filename);

    let contents = fs::read_to_string(filename).expect("failed to read file");
    part1(&contents);
    part2(&contents);
}
