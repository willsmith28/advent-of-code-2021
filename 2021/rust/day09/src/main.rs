use std::collections::hash_set;
use std::env;
use std::fs;

fn part1(input_data: &Vec<Vec<u32>>) -> Vec<(usize, usize)> {
    let mut low_points = vec![];
    let mut heights = vec![];

    let last_row_index = input_data.len() - 1;
    let last_column_index = input_data[0].len() - 1;

    for row in 0..input_data.len() {
        for column in 0..input_data[0].len() {
            let height = input_data[row][column];
            match (row, column) {
                // top left corner
                (0, 0) => {
                    if height < input_data[0][1] && height < input_data[1][0] {
                        low_points.push((row, column));
                        heights.push(height);
                    }
                }
                // top right corner
                (0, j) if j == last_column_index => {
                    if height < input_data[1][j] && height < input_data[0][j - 1] {
                        low_points.push((row, column));
                        heights.push(height);
                    }
                }
                // bottom left corner
                (i, 0) if i == last_row_index => {
                    if height < input_data[i - 1][0] && height < input_data[i][1] {
                        low_points.push((row, column));
                        heights.push(height);
                    }
                }
                // bottom right corner
                (i, j) if i == last_row_index && j == last_column_index => {
                    if height < input_data[i - 1][j] && height < input_data[i][j - 1] {
                        low_points.push((row, column));
                        heights.push(height);
                    }
                }
                // right side non corner
                (i, j) if j == last_column_index => {
                    if height < input_data[i + 1][j]
                        && height < input_data[i - 1][j]
                        && height < input_data[i][j - 1]
                    {
                        low_points.push((row, column));
                        heights.push(height);
                    }
                }
                // left side non corner
                (i, 0) => {
                    if height < input_data[i + 1][0]
                        && height < input_data[i - 1][0]
                        && height < input_data[i][1]
                    {
                        low_points.push((row, column));
                        heights.push(height);
                    }
                }
                // top row non corners
                (0, j) => {
                    if height < input_data[1][j]
                        && height < input_data[0][j + 1]
                        && height < input_data[0][j - 1]
                    {
                        low_points.push((row, column));
                        heights.push(height);
                    }
                }
                // bottom row non corner
                (i, j) if i == last_row_index => {
                    if height < input_data[i - 1][j]
                        && height < input_data[i][j + 1]
                        && height < input_data[i][j - 1]
                    {
                        low_points.push((row, column));
                        heights.push(height);
                    }
                }
                (i, j) => {
                    if height < input_data[i + 1][j]
                        && height < input_data[i - 1][j]
                        && height < input_data[i][j + 1]
                        && height < input_data[i][j - 1]
                    {
                        low_points.push((row, column));
                        heights.push(height);
                    }
                }
            }
        }
    }

    let total_risk: u32 = heights.into_iter().map(|height| height + 1).sum();
    println!("{}", total_risk);
    return low_points;
}

fn part2(input_data: &Vec<Vec<u32>>, local_minimums: &Vec<(usize, usize)>) {
    let mut visited: hash_set::HashSet<(usize, usize)> = hash_set::HashSet::new();
    let mut basins: Vec<usize> = local_minimums
        .iter()
        .map(|point| {
            count_basin(
                point.0 as isize,
                point.1 as isize,
                &mut visited,
                &input_data,
            )
        })
        .collect();
    basins.sort();
    let total: usize = basins.iter().rev().take(3).product();
    println!("{}", total);
}

fn count_basin(
    row: isize,
    column: isize,
    visited: &mut hash_set::HashSet<(usize, usize)>,
    grid: &Vec<Vec<u32>>,
) -> usize {
    let point = (row as usize, column as usize);
    if visited.contains(&point) || row < 0 || column < 0 {
        return 0;
    }

    // nested match is a little ugly
    // is there a better way?
    return match grid.get(point.0) {
        Some(line) => match line.get(point.1) {
            Some(9) => 0,
            Some(_) => {
                visited.insert(point);
                1 + count_basin(row, column + 1, visited, grid)
                    + count_basin(row, column - 1, visited, grid)
                    + count_basin(row + 1, column, visited, grid)
                    + count_basin(row - 1, column, visited, grid)
            }
            None => 0,
        },
        None => 0,
    };
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    println!("input file {}", filename);
    let contents = fs::read_to_string(filename).expect("failed to read file");
    let input_data: Vec<Vec<u32>> = contents
        .lines()
        .filter(|line| *line != "\n")
        .map(|line| {
            line.chars()
                .filter(|_char| *_char != '\n')
                .map(|_char| _char.to_digit(10).unwrap())
                .collect()
        })
        .collect();

    let low_points = part1(&input_data);
    part2(&input_data, &low_points);
}
