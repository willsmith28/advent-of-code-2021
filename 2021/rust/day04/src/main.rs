use std::env;
use std::fs;

fn get_draws_and_boards(contents: &str) -> (Vec<&str>, Vec<Vec<Vec<&str>>>) {
    let input_data: Vec<&str> = contents.lines().collect();
    let mut input_iter = input_data.iter();

    let draws: Vec<&str> = input_iter
        .next()
        .expect("first line is the draw list")
        .split(",")
        .collect();

    let mut boards: Vec<Vec<Vec<&str>>> = Vec::new();
    let mut board: Vec<Vec<&str>> = Vec::new();
    input_iter.next(); // skip newline after draws but before boards
    for &line in input_iter {
        let split_lines: Vec<&str> = line.split_whitespace().collect();
        if split_lines.len() == 0 {
            boards.push(board.to_vec());
            board.clear();
        } else {
            board.push(split_lines.to_vec());
        }
    }

    (draws, boards)
}

fn search_for_win(board: &Vec<Vec<&str>>, draws: &Vec<&str>) -> (usize, Vec<Vec<bool>>) {
    let mut state: Vec<Vec<bool>> = (0..board.len())
        .map(|_| (0..board[0].len()).map(|_| false).collect())
        .collect();

    for (turn, &draw) in draws.iter().enumerate() {
        // change state based on draw
        for row in 0..board.len() {
            for column in 0..board[0].len() {
                if draw == board[row][column] {
                    state[row][column] = true;
                }
            }
        }

        // search for win
        for row in state.iter() {
            if row.iter().all(|&item| item) {
                return (turn, state);
            }
        }

        for column_index in 0..state[0].len() {
            if state.iter().map(|row| row[column_index]).all(|item| item) {
                return (turn, state);
            }
        }
    }

    panic!("This board has no win based on given draws");
}

fn part1(draws: Vec<&str>, boards: Vec<Vec<Vec<&str>>>) {
    let wins: Vec<(usize, Vec<Vec<bool>>)> = boards
        .iter()
        .filter(|board| board.len() > 0)
        .map(|board| search_for_win(board, &draws))
        .collect();

    let winning_turn_numbers: Vec<usize> = wins.iter().map(|(turn, _)| *turn).collect();
    let smallest_winning_turn = winning_turn_numbers.iter().min().unwrap();

    let winning_board_index = winning_turn_numbers
        .iter()
        .position(|&item| item == *smallest_winning_turn)
        .unwrap();

    let board = &boards[winning_board_index];
    let (draw_index, state) = &wins[winning_board_index];

    let mut score = 0;
    for row in 0..board.len() {
        for column in 0..board[0].len() {
            if !state[row][column] {
                score += board[row][column].parse::<i32>().unwrap()
            }
        }
    }
    let winning_draw = draws
        .iter()
        .nth(*draw_index)
        .unwrap()
        .parse::<i32>()
        .unwrap();

    println!("{}", score * winning_draw)
}

fn part2(draws: Vec<&str>, boards: Vec<Vec<Vec<&str>>>) {
    let wins: Vec<(usize, Vec<Vec<bool>>)> = boards
        .iter()
        .filter(|board| board.len() > 0)
        .map(|board| search_for_win(board, &draws))
        .collect();

    let winning_turn_numbers: Vec<usize> = wins.iter().map(|(turn, _)| *turn).collect();
    let last_winning_board_index = winning_turn_numbers
        .iter()
        .position(|&item| item == *winning_turn_numbers.iter().max().unwrap())
        .unwrap();

    let board = &boards[last_winning_board_index];
    let (draw_index, state) = &wins[last_winning_board_index];

    let mut score = 0;
    for row in 0..board.len() {
        for column in 0..board[0].len() {
            if !state[row][column] {
                score += board[row][column].parse::<i32>().unwrap()
            }
        }
    }
    let winning_draw = draws
        .iter()
        .nth(*draw_index)
        .unwrap()
        .parse::<i32>()
        .unwrap();

    println!("{}", score * winning_draw)
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    println!("input file {}", filename);
    let contents = fs::read_to_string(filename).expect("failed to read file");
    let (draws, boards) = get_draws_and_boards(&contents);
    part1(draws.to_vec(), boards.to_vec());
    part2(draws.to_vec(), boards.to_vec());
}
