import pathlib
import re
import itertools
import functools
from collections import Counter
from collections.abc import Iterator, Generator
from typing import TypedDict


class Player(TypedDict):
    player_id: int
    current_position: int
    score: int


NUM_SPACES = 10


def part1(player1: Player, player2: Player):
    def deterministic_dice() -> Generator[int, None, None]:
        for i in itertools.cycle(range(1, 101)):
            yield i

    def take_turn(
        player: Player,
        dice_rolls: Iterator[int],
    ):
        total_moves = sum(next(dice_rolls) for _ in range(3))
        new_postion = (player["current_position"] + total_moves) % NUM_SPACES
        if new_postion == 0:
            new_postion = NUM_SPACES

        player["current_position"] = new_postion
        player["score"] += new_postion

    winning_score = 1_000
    loser = None
    dice_rolls = deterministic_dice()
    total_rolls = 0
    while loser is None:
        take_turn(player1, dice_rolls)
        total_rolls += 3
        if player1["score"] >= winning_score:
            loser = player2
            break

        take_turn(player2, dice_rolls)
        total_rolls += 3
        if player2["score"] >= winning_score:
            loser = player1

    print(loser["score"] * total_rolls)


@functools.cache
def move_pawn(current_position: int, total_roll: int):
    new_position = (current_position + total_roll) % NUM_SPACES
    if new_position == 0:
        new_position = NUM_SPACES

    return new_position


dice_outcomes = Counter(sum(rolls) for rolls in itertools.product((1, 2, 3), repeat=3))


@functools.cache
def multidimentional_game(
    player1_position: int, player1_score: int, player2_positin: int, player2_score: int
):
    if player1_score >= 21:
        return (1, 0)

    if player2_score >= 21:
        return (0, 1)

    player1_total_wins = 0
    player2_total_wins = 0
    for total_movement, number_of_universes in dice_outcomes.items():
        player1_new_position = move_pawn(player1_position, total_movement)
        player1_new_score = player1_score + player1_new_position

        player2_wins, player1_wins = multidimentional_game(
            player2_positin, player2_score, player1_new_position, player1_new_score
        )
        player1_total_wins += player1_wins * number_of_universes
        player2_total_wins += player2_wins * number_of_universes

    return player1_total_wins, player2_total_wins


def part2(player1_position: int, player2_position: int):
    print(max(multidimentional_game(player1_position, 0, player2_position, 0)))


def main():
    players = []
    with open(pathlib.Path(__file__).parent.parent / "input" / "day21") as input_file:
        for line in input_file.readlines():
            if match := re.search(r"Player (\d+) starting position: (\d+)", line):
                players.append(
                    Player(
                        current_position=int(match.group(2)),
                        score=0,
                        player_id=int(match.group(1)),
                    )
                )

    player1, player2 = players
    # part1(player1, player2)
    part2(player1["current_position"], player2["current_position"])


if __name__ == "__main__":
    main()
