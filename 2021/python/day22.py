import pathlib
import re


def main():
    input_data = []
    with open(pathlib.Path(__file__).parent.parent / "input" / "day22") as input_file:
        for line in input_file.readlines():
            if match := re.search(
                r"(on|off)\sx=(\-?\d+..\-?\d+),y=(\-?\d+..\-?\d+),z=(\-?\d+..\-?\d+)",
                line,
            ):
                input_data.append(match.groups())


if __name__ == "__main__":
    main()
