import functools
import heapq
import pathlib
from collections import defaultdict
from typing import TypeAlias, TypedDict

Position: TypeAlias = tuple[int, int]


class Node(TypedDict):
    position: Position
    weight: int


Graph: TypeAlias = dict[Position, list[Node]]


class PriorityQueue:
    def __init__(self) -> None:
        self.elements: list[tuple[float, Position]] = []

    @property
    def is_empty(self) -> bool:
        return not self.elements

    def push(self, item: Position, priority: float):
        heapq.heappush(self.elements, (priority, item))

    def pop(self) -> Position:
        return heapq.heappop(self.elements)[1]


def create_graph(
    input_data: list[list[int]], factor: int = 1, print_grid: bool = False
) -> Graph:
    @functools.cache
    def get_weight(position: Position) -> int:
        row, column = position
        vertical_tile = row // len(input_data)
        horizontal_tile = column // len(input_data[0])
        origin_row = row % len(input_data)
        origin_column = column % len(input_data[0])
        weight = input_data[origin_row][origin_column] + vertical_tile + horizontal_tile
        if weight > 9:
            weight %= 9

        return weight

    graph = defaultdict(list)
    grid_row_length = len(input_data) * factor
    grid_column_length = len(input_data[0]) * factor
    for row_index in range(grid_row_length):
        for column_index in range(grid_column_length):
            node_position = (row_index, column_index)

            if print_grid:
                print(
                    get_weight(node_position),
                    end="",
                )

            neighboring_positions = (
                (row_index, column_index + 1),  # right
                (row_index, column_index - 1),  # left
                (row_index + 1, column_index),  # top
                (row_index - 1, column_index),  # bottom
            )
            for neighbor_row, neighbor_column in neighboring_positions:
                if (
                    neighbor_row < 0
                    or neighbor_column < 0
                    or neighbor_row >= grid_row_length
                    or neighbor_column >= grid_column_length
                ):
                    continue

                neighbor_position = (neighbor_row, neighbor_column)

                graph[node_position].append(
                    Node(
                        position=neighbor_position,
                        weight=get_weight(neighbor_position),
                    )
                )

        if print_grid:
            print("", end="\n")

    return graph


def reconstruct_path(
    came_from: dict[Position, Position], start: Position, goal: Position
) -> list[Position]:
    current = goal
    path = []
    while current != start:
        path.append(current)
        current = came_from[current]

    return path


def search(
    graph: dict[Position, list[Node]],
    start: Position,
    goal: Position,
):
    frontier = PriorityQueue()
    frontier.push(start, 0)
    came_from: dict[Position, Position | None] = {start: None}
    cost_from_start: dict[Position, float] = {start: 0}

    while not frontier.is_empty:
        current_node = frontier.pop()

        if current_node == goal:
            break

        neighbors = graph[current_node]
        for next_node in neighbors:
            next_cost = cost_from_start[current_node] + next_node["weight"]
            if (
                next_node["position"] not in cost_from_start
                or next_cost < cost_from_start[next_node["position"]]
            ):
                cost_from_start[next_node["position"]] = next_cost
                frontier.push(next_node["position"], next_cost)
                came_from[next_node["position"]] = current_node

    return came_from, cost_from_start


def part1(risk_levels: list[list[int]]):
    """
    --- Day 15: Chiton ---

    You've almost reached the exit of the cave, but the walls are getting closer together. Your submarine can barely still fit, though; the main problem is that the walls of the cave are covered in chitons, and it would be best not to bump any of them.

    The cavern is large, but has a very low ceiling, restricting your motion to two dimensions. The shape of the cavern resembles a square; a quick scan of chiton density produces a map of risk level throughout the cave (your puzzle input). For example:

    1163751742
    1381373672
    2136511328
    3694931569
    7463417111
    1319128137
    1359912421
    3125421639
    1293138521
    2311944581

    You start in the top left position, your destination is the bottom right position, and you cannot move diagonally. The number at each position is its risk level; to determine the total risk of an entire path, add up the risk levels of each position you enter (that is, don't count the risk level of your starting position unless you enter it; leaving it adds no risk to your total).

    Your goal is to find a path with the lowest total risk.
    The total risk of this path is 40 (the starting position is never entered, so its risk is not counted).

    What is the lowest total risk of any path from the top left to the bottom right?
    """
    start = (0, 0)
    end = (len(risk_levels) - 1, len(risk_levels[0]) - 1)
    graph = create_graph(risk_levels)
    path_mapping, risk_mapping = search(graph, start, end)
    print(risk_mapping[end])


def part2(risk_levels: list[list[int]]):
    """
    Now that you know how to find low-risk paths in the cave, you can try to find your way out.

    The entire cave is actually five times larger in both dimensions than you thought; the area you originally scanned is just one tile in a 5x5 tile area that forms the full map. Your original map tile repeats to the right and downward; each time the tile repeats to the right or downward, all of its risk levels are 1 higher than the tile immediately up or left of it. However, risk levels above 9 wrap back around to 1. So, if your original map had some position with a risk level of 8, then that same position on each of the 25 total tiles would be as follows:

    8 9 1 2 3
    9 1 2 3 4
    1 2 3 4 5
    2 3 4 5 6
    3 4 5 6 7

    Each single digit above corresponds to the example position with a value of 8 on the top-left tile. Because the full map is actually five times larger in both dimensions, that position appears a total of 25 times, once in each duplicated tile, with the values shown above.

    Here is the full five-times-as-large version of the first example above, with the original map in the top left corner highlighted:

    11637517422274862853338597396444961841755517295286
    13813736722492484783351359589446246169155735727126
    21365113283247622439435873354154698446526571955763
    36949315694715142671582625378269373648937148475914
    74634171118574528222968563933317967414442817852555
    13191281372421239248353234135946434524615754563572
    13599124212461123532357223464346833457545794456865
    31254216394236532741534764385264587549637569865174
    12931385212314249632342535174345364628545647573965
    23119445813422155692453326671356443778246755488935
    22748628533385973964449618417555172952866628316397
    24924847833513595894462461691557357271266846838237
    32476224394358733541546984465265719557637682166874
    47151426715826253782693736489371484759148259586125
    85745282229685639333179674144428178525553928963666
    24212392483532341359464345246157545635726865674683
    24611235323572234643468334575457944568656815567976
    42365327415347643852645875496375698651748671976285
    23142496323425351743453646285456475739656758684176
    34221556924533266713564437782467554889357866599146
    33859739644496184175551729528666283163977739427418
    35135958944624616915573572712668468382377957949348
    43587335415469844652657195576376821668748793277985
    58262537826937364893714847591482595861259361697236
    96856393331796741444281785255539289636664139174777
    35323413594643452461575456357268656746837976785794
    35722346434683345754579445686568155679767926678187
    53476438526458754963756986517486719762859782187396
    34253517434536462854564757396567586841767869795287
    45332667135644377824675548893578665991468977611257
    44961841755517295286662831639777394274188841538529
    46246169155735727126684683823779579493488168151459
    54698446526571955763768216687487932779859814388196
    69373648937148475914825958612593616972361472718347
    17967414442817852555392896366641391747775241285888
    46434524615754563572686567468379767857948187896815
    46833457545794456865681556797679266781878137789298
    64587549637569865174867197628597821873961893298417
    45364628545647573965675868417678697952878971816398
    56443778246755488935786659914689776112579188722368
    55172952866628316397773942741888415385299952649631
    57357271266846838237795794934881681514599279262561
    65719557637682166874879327798598143881961925499217
    71484759148259586125936169723614727183472583829458
    28178525553928963666413917477752412858886352396999
    57545635726865674683797678579481878968159298917926
    57944568656815567976792667818781377892989248891319
    75698651748671976285978218739618932984172914319528
    56475739656758684176786979528789718163989182927419
    67554889357866599146897761125791887223681299833479

    Equipped with the full map, you can now find a path from the top left corner to the bottom right corner with the lowest total risk:

    The total risk of this path is 315 (the starting position is still never entered, so its risk is not counted).

    Using the full map, what is the lowest total risk of any path from the top left to the bottom right?
    """
    graph = create_graph(risk_levels, factor=5)
    start = (0, 0)
    end = (len(risk_levels) * 5 - 1, len(risk_levels[0]) * 5 - 1)
    path_mapping, risk_mapping = search(graph, start, end)
    print(risk_mapping[end])


def main():
    with open(pathlib.Path(__file__).parent.parent / "input" / "day15") as input_file:
        input_data = [list(map(int, line.strip())) for line in input_file.readlines()]

    part1(input_data)
    part2(input_data)


if __name__ == "__main__":
    main()
