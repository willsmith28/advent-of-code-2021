import pathlib
from collections import defaultdict


def get_input_data_from_file() -> tuple[str, dict[str, tuple[str, str]]]:
    with open(pathlib.Path(__file__).parent.parent / "input" / "day14") as input_file:
        template = input_file.readline().strip()
        input_file.readline()  # skip newline
        pair_insertion_expansions = {}
        for rule in input_file.readlines():
            pair, new = rule.strip().split(" -> ")
            pair_insertion_expansions[pair] = f"{pair[0]}{new}", f"{new}{pair[1]}"

        return template, pair_insertion_expansions


def insert_polymers(
    polymer_counts: dict[str, int],
    pair_insertion_expansions: dict[str, tuple[str, str]],
) -> defaultdict[str, int]:
    new_ploymer_counts = defaultdict(int)
    for polymer, count in polymer_counts.items():
        pair1, pair2 = pair_insertion_expansions[polymer]
        new_ploymer_counts[pair1] += count
        new_ploymer_counts[pair2] += count

    return new_ploymer_counts


def count_elements(polymer_counts: dict[str, int]):
    element_count = defaultdict(int)
    for pair, count in polymer_counts.items():
        element1, element2 = pair
        element_count[element1] += count
        element_count[element2] += count

    # the element count is cut in half with upside-down floor integer division
    # resulting in an integer cieling
    return {element: -(count // -2) for element, count in element_count.items()}


def part1(
    polymer_counts: dict[str, int],
    pair_insertion_expansions: dict[str, tuple[str, str]],
):
    """
    --- Day 14: Extended Polymerization ---

    The incredible pressures at this depth are starting to put a strain on your submarine. The submarine has polymerization equipment that would produce suitable materials to reinforce the submarine, and the nearby volcanically-active caves should even have the necessary input elements in sufficient quantities.

    The submarine manual contains instructions for finding the optimal polymer formula; specifically, it offers a polymer template and a list of pair insertion rules (your puzzle input). You just need to work out what polymer would result after repeating the pair insertion process a few times.

    For example:

    NNCB

    CH -> B
    HH -> N
    CB -> H
    NH -> C
    HB -> C
    HC -> B
    HN -> C
    NN -> C
    BH -> H
    NC -> B
    NB -> B
    BN -> B
    BB -> N
    BC -> B
    CC -> N
    CN -> C

    The first line is the polymer template - this is the starting point of the process.

    The following section defines the pair insertion rules. A rule like AB -> C means that when elements A and B are immediately adjacent, element C should be inserted between them. These insertions all happen simultaneously.

    So, starting with the polymer template NNCB, the first step simultaneously considers all three pairs:

        The first pair (NN) matches the rule NN -> C, so element C is inserted between the first N and the second N.
        The second pair (NC) matches the rule NC -> B, so element B is inserted between the N and the C.
        The third pair (CB) matches the rule CB -> H, so element H is inserted between the C and the B.

    Note that these pairs overlap: the second element of one pair is the first element of the next pair. Also, because all pairs are considered simultaneously, inserted elements are not considered to be part of a pair until the next step.

    After the first step of this process, the polymer becomes NCNBCHB.

    Here are the results of a few steps using the above rules:

    Template:     NNCB
    After step 1: NCNBCHB
    After step 2: NBCCNBBBCBHCB
    After step 3: NBBBCNCCNBBNBNBBCHBHHBCHB
    After step 4: NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB

    This polymer grows quickly. After step 5, it has length 97; After step 10, it has length 3073. After step 10, B occurs 1749 times, C occurs 298 times, H occurs 161 times, and N occurs 865 times; taking the quantity of the most common element (B, 1749) and subtracting the quantity of the least common element (H, 161) produces 1749 - 161 = 1588.

    Apply 10 steps of pair insertion to the polymer template and find the most and least common elements in the result. What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?
    """
    for _ in range(10):
        polymer_counts = insert_polymers(polymer_counts, pair_insertion_expansions)

    element_counts = count_elements(polymer_counts)

    most_common_elemnt_count = max(value for value in element_counts.values())
    least_common_element_count = min(value for value in element_counts.values())
    print(most_common_elemnt_count - least_common_element_count)

    return polymer_counts


def part2(
    polymer_counts: dict[str, int],
    pair_insertion_expansions: dict[str, tuple[str, str]],
):
    """
    The resulting polymer isn't nearly strong enough to reinforce the submarine. You'll need to run more steps of the pair insertion process; a total of 40 steps should do it.

    In the above example, the most common element is B (occurring 2192039569602 times) and the least common element is H (occurring 3849876073 times); subtracting these produces 2188189693529.

    Apply 40 steps of pair insertion to the polymer template and find the most and least common elements in the result. What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?
    """
    for _ in range(30):
        polymer_counts = insert_polymers(polymer_counts, pair_insertion_expansions)

    element_counts = count_elements(polymer_counts)

    most_common_elemnt_count = max(value for value in element_counts.values())
    least_common_element_count = min(value for value in element_counts.values())
    print(most_common_elemnt_count - least_common_element_count)


def main():
    polymer_counts = defaultdict(int)
    template, pair_insertion_expansions = get_input_data_from_file()
    # count polymers in template
    window_size = 2
    for i in range(len(template) - window_size + 1):
        polymer = []
        for j in range(window_size):
            polymer.append(template[i + j])

        polymer = "".join(polymer)
        polymer_counts[polymer] += 1

    polymer_counts = part1(polymer_counts, pair_insertion_expansions)
    part2(polymer_counts, pair_insertion_expansions)


if __name__ == "__main__":
    main()
