import itertools
import math
import pathlib
import re
from pprint import pprint
from typing import TypeAlias, TypedDict

Position: TypeAlias = tuple[int, int, int]


class Scanner(TypedDict):
    id: int
    beacons: list[Position]
    fingerprint: set[float]
    vectors: set[tuple[int, int, int]]
    absolute_position: Position


X, Y, Z = 0, 1, 2

ROTATIONS = (
    lambda x, y, z: (x, y, z),
    lambda x, y, z: (y, z, x),
    lambda x, y, z: (z, x, y),
    lambda x, y, z: (-x, z, y),
    lambda x, y, z: (z, y, -x),
    lambda x, y, z: (y, -x, z),
    lambda x, y, z: (x, z, -y),
    lambda x, y, z: (z, -y, x),
    lambda x, y, z: (-y, x, z),
    lambda x, y, z: (x, -z, y),
    lambda x, y, z: (-z, y, x),
    lambda x, y, z: (y, x, -z),
    lambda x, y, z: (-x, -y, z),
    lambda x, y, z: (-y, z, -x),
    lambda x, y, z: (z, -x, -y),
    lambda x, y, z: (-x, y, -z),
    lambda x, y, z: (y, -z, -x),
    lambda x, y, z: (-z, -x, y),
    lambda x, y, z: (x, -y, -z),
    lambda x, y, z: (-y, -z, x),
    lambda x, y, z: (-z, x, -y),
    lambda x, y, z: (-x, -z, -y),
    lambda x, y, z: (-z, -y, -x),
    lambda x, y, z: (-y, -x, -z),
)


def vector(starting_point: Position, terminal_point: Position) -> tuple[int, int, int]:
    return (
        terminal_point[X] - starting_point[X],
        terminal_point[Y] - starting_point[Y],
        terminal_point[Z] - starting_point[Z],
    )


def magnitude(point1: Position, point2: Position) -> float:
    return math.sqrt(
        (point1[X] - point2[X]) ** 2
        + (point1[Y] - point2[Y]) ** 2
        + (point1[Z] - point2[Z]) ** 2
    )


def manhattan_distance(p1: Position, p2: Position):
    return abs(p1[X] - p2[X]) + abs(p1[Y] - p2[Y]) + abs(p1[Z] - p2[Z])


def translate_beacons(
    absolute_beacons: list[Position], relative_beacons: list[Position]
) -> tuple[list[Position], Position]:
    absolute_vector_mapping = {
        vector(*beacon_pair): beacon_pair[0]
        for beacon_pair in itertools.combinations(absolute_beacons, r=2)
    }
    relative_vector_mapping = {
        vector(*beacon_pair): beacon_pair[0]
        for beacon_pair in itertools.combinations(relative_beacons, r=2)
    }
    intersecting_vectors = set(absolute_vector_mapping.keys()).intersection(
        relative_vector_mapping.keys()
    )
    assert len(intersecting_vectors) >= 66
    intersecting_vector = next(iter(intersecting_vectors))
    absolute_starting = absolute_vector_mapping[intersecting_vector]
    relative_starting = relative_vector_mapping[intersecting_vector]
    absolute_scanner_position = (
        absolute_starting[X] - relative_starting[X],
        absolute_starting[Y] - relative_starting[Y],
        absolute_starting[Z] - relative_starting[Z],
    )

    return (
        sorted(
            (
                absolute_scanner_position[X] + beacon[X],
                absolute_scanner_position[Y] + beacon[Y],
                absolute_scanner_position[Z] + beacon[Z],
            )
            for beacon in relative_beacons
        ),
        absolute_scanner_position,
    )


def fingerprint_beacons(beacons: list[Position]) -> set[float]:
    return {
        magnitude(beacon1, beacon2)
        for beacon1, beacon2 in itertools.combinations(beacons, r=2)
    }


def find_scanner_rotation(
    known_scanners: dict[int, Scanner], relative_scanner: Scanner
) -> Scanner:
    for rotation_func in ROTATIONS:
        rotated_beacons: list[Position] = sorted(
            rotation_func(*beacon) for beacon in relative_scanner["beacons"]
        )
        rotated_vectors = {
            vector(*beacon_pair)
            for beacon_pair in itertools.combinations(rotated_beacons, r=2)
        }
        for scanner in known_scanners.values():
            if len(scanner["vectors"] & rotated_vectors) >= 66:
                absolute_beacons, absolute_scanner_position = translate_beacons(
                    scanner["beacons"], rotated_beacons
                )
                return Scanner(
                    id=relative_scanner["id"],
                    beacons=absolute_beacons,
                    fingerprint=relative_scanner["fingerprint"],
                    vectors=rotated_vectors,
                    absolute_position=absolute_scanner_position,
                )

    raise ValueError("No matching rotations")


def part1(scanners: dict[int, Scanner]):
    total_scanners = len(scanners)
    overlapping: dict[int, list[int]] = {}
    for scanner1, scanner2 in itertools.permutations(scanners.values(), r=2):
        if len(scanner1["fingerprint"] & scanner2["fingerprint"]) >= 66:
            overlapping.setdefault(scanner1["id"], []).append(scanner2["id"])

    known_scanners = {0: scanners[0]}
    stack = [0]
    while stack:
        overlapping_scanners = (
            scanners[id_]
            for id_ in overlapping[stack.pop()]
            if id_ not in known_scanners
        )
        for scanner in overlapping_scanners:
            new_absolute_scanner = find_scanner_rotation(known_scanners, scanner)

            known_scanners[scanner["id"]] = new_absolute_scanner
            stack.append(new_absolute_scanner["id"])

    assert len(known_scanners) == total_scanners
    print(
        len(
            set(
                itertools.chain.from_iterable(
                    scanner["beacons"] for scanner in known_scanners.values()
                )
            )
        )
    )
    return list(known_scanners.values())


def part2(scanners: list[Scanner]):
    print(
        max(
            manhattan_distance(s1["absolute_position"], s2["absolute_position"])
            for s1, s2 in itertools.combinations(scanners, r=2)
        )
    )


def main():
    input_data = {}
    with open(pathlib.Path(__file__).parent.parent / "input" / "day19") as input_file:
        scanner = None
        for line in input_file.readlines():
            if match := re.search("--- scanner (\d+)", line):
                scanner = int(match.group(1))
                continue

            elif line == "\n":
                continue

            input_data.setdefault(scanner, []).append(
                tuple(map(int, line.strip().split(",")))
            )

    scanners = {
        scanner: Scanner(
            beacons=(ordered_beacons := sorted(beacon_list)),
            fingerprint={
                magnitude(*beacons)
                for beacons in itertools.combinations(ordered_beacons, r=2)
            },
            vectors={
                vector(*beacons)
                for beacons in itertools.combinations(ordered_beacons, r=2)
            },
            id=scanner,
            absolute_position=(0, 0, 0),
        )
        for scanner, beacon_list in input_data.items()
    }
    absolute_scanners = part1(scanners)
    part2(absolute_scanners)


if __name__ == "__main__":
    main()
