import pathlib
from typing import TypeAlias

Position: TypeAlias = tuple[int, int]
Image: TypeAlias = dict[Position, bool]


def get_enhancement_index_from_position(
    image: Image, position: Position, infinite_position_default
) -> int:
    x, y = position
    square = [
        str(int(image.get((x - 1, y - 1), infinite_position_default))),
        str(int(image.get((x, y - 1), infinite_position_default))),
        str(int(image.get((x + 1, y - 1), infinite_position_default))),
        str(int(image.get((x - 1, y), infinite_position_default))),
        str(int(image.get((x, y), infinite_position_default))),
        str(int(image.get((x + 1, y), infinite_position_default))),
        str(int(image.get((x - 1, y + 1), infinite_position_default))),
        str(int(image.get((x, y + 1), infinite_position_default))),
        str(int(image.get((x + 1, y + 1), infinite_position_default))),
    ]
    return int("".join(pixel for pixel in square), base=2)


def enhance_image(
    image: Image, enhancement_algorithm: str, infinite_position_default: bool
) -> Image:
    min_x = min(x for (x, _) in image.keys())
    max_x = max(x for (x, _) in image.keys())
    min_y = min(y for (_, y) in image.keys())
    max_y = max(y for (_, y) in image.keys())
    new_image: Image = {
        (x, y): enhancement_algorithm[
            get_enhancement_index_from_position(
                image, (x, y), infinite_position_default=infinite_position_default
            )
        ]
        == "#"
        for y in range(min_y - 1, max_y + 2)
        for x in range(min_x - 2, max_x + 2)
    }

    return new_image


def print_image(image: Image):
    min_x = min(x for (x, _) in image.keys())
    max_x = max(x for (x, _) in image.keys())
    min_y = min(y for (_, y) in image.keys())
    max_y = max(y for (_, y) in image.keys())
    for y in range(min_y, max_y + 1):
        for x in range(min_x, max_x + 1):
            pixel = image[(x, y)]
            print("#" if pixel else ".", end="")

        print("\n", end="")


def iterate_enhancements(image: Image, enhancement_algorithm: str, n: int):
    infinite_position_default = False
    for _ in range(n):
        image = enhance_image(image, enhancement_algorithm, infinite_position_default)
        if not infinite_position_default and enhancement_algorithm[0] == "#":
            # all infinite empty 3 x 3 grids turn lit
            infinite_position_default = True
        elif infinite_position_default and enhancement_algorithm[511] == ".":
            # all infinite lit 3 x 3 grids turn empty
            infinite_position_default = False

    assert not infinite_position_default
    return image


def part1(image: Image, enhancement_algorithm: str):
    """
    --- Day 20: Trench Map ---

    With the scanners fully deployed, you turn their attention to mapping the floor of the ocean trench.

    When you get back the image from the scanners, it seems to just be random noise. Perhaps you can combine an image enhancement algorithm and the input image (your puzzle input) to clean it up a little.

    For example:

    ..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
    #..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###
    .######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#.
    .#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#.....
    .#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#..
    ...####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.....
    ..##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

    #..#.
    #....
    ##..#
    ..#..
    ..###

    The first section is the image enhancement algorithm. It is normally given on a single line, but it has been wrapped to multiple lines in this example for legibility. The second section is the input image, a two-dimensional grid of light pixels (#) and dark pixels (.).

    The image enhancement algorithm describes how to enhance an image by simultaneously converting all pixels in the input image into an output image. Each pixel of the output image is determined by looking at a 3x3 square of pixels centered on the corresponding input image pixel. So, to determine the value of the pixel at (5,10) in the output image, nine pixels from the input image need to be considered: (4,9), (4,10), (4,11), (5,9), (5,10), (5,11), (6,9), (6,10), and (6,11). These nine input pixels are combined into a single binary number that is used as an index in the image enhancement algorithm string.

    For example, to determine the output pixel that corresponds to the very middle pixel of the input image, the nine pixels marked by [...] would need to be considered:

    # . . # .
    #[. . .].
    #[# . .]#
    .[. # .].
    . . # # #

    Starting from the top-left and reading across each row, these pixels are ..., then #.., then .#.; combining these forms ...#...#.. By turning dark pixels (.) into 0 and light pixels (#) into 1, the binary number 000100010 can be formed, which is 34 in decimal.

    The image enhancement algorithm string is exactly 512 characters long, enough to match every possible 9-bit binary number. The first few characters of the string (numbered starting from zero) are as follows:

    0         10        20        30  34    40        50        60        70
    |         |         |         |   |     |         |         |         |
    ..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##

    In the middle of this first group of characters, the character at index 34 can be found: #. So, the output pixel in the center of the output image should be #, a light pixel.

    This process can then be repeated to calculate every pixel of the output image.

    Through advances in imaging technology, the images being operated on here are infinite in size. Every pixel of the infinite output image needs to be calculated exactly based on the relevant pixels of the input image. The small input image you have is only a small region of the actual infinite input image; the rest of the input image consists of dark pixels (.). For the purposes of the example, to save on space, only a portion of the infinite-sized input and output images will be shown.

    The starting input image, therefore, looks something like this, with more dark pixels (.) extending forever in every direction not shown here:

    ...............
    ...............
    ...............
    ...............
    ...............
    .....#..#......
    .....#.........
    .....##..#.....
    .......#.......
    .......###.....
    ...............
    ...............
    ...............
    ...............
    ...............

    By applying the image enhancement algorithm to every pixel simultaneously, the following output image can be obtained:

    ...............
    ...............
    ...............
    ...............
    .....##.##.....
    ....#..#.#.....
    ....##.#..#....
    ....####..#....
    .....#..##.....
    ......##..#....
    .......#.#.....
    ...............
    ...............
    ...............
    ...............

    Through further advances in imaging technology, the above output image can also be used as an input image! This allows it to be enhanced a second time:

    ...............
    ...............
    ...............
    ..........#....
    ....#..#.#.....
    ...#.#...###...
    ...#...##.#....
    ...#.....#.#...
    ....#.#####....
    .....#.#####...
    ......##.##....
    .......###.....
    ...............
    ...............
    ...............

    Truly incredible - now the small details are really starting to come through. After enhancing the original input image twice, 35 pixels are lit.

    Start with the original input image and apply the image enhancement algorithm twice, being careful to account for the infinite size of the images. How many pixels are lit in the resulting image?
    """
    image = iterate_enhancements(image, enhancement_algorithm, 2)
    print(sum(pixel for pixel in image.values()))


def part2(image: Image, enhancement_algorithm: str):
    """
    You still can't quite make out the details in the image. Maybe you just didn't enhance it enough.

    If you enhance the starting input image in the above example a total of 50 times, 3351 pixels are lit in the final output image.

    Start again with the original input image and apply the image enhancement algorithm 50 times. How many pixels are lit in the resulting image?
    """
    image = iterate_enhancements(image, enhancement_algorithm, 50)
    print(sum(pixel for pixel in image.values()))


def main():
    with open(pathlib.Path(__file__).parent.parent / "input" / "day20") as input_file:
        enhancement_algorithm = input_file.readline().strip()
        input_file.readline()
        input_image: Image = {
            (x, y): char == "#"
            for y, line in enumerate(input_file.readlines())
            for x, char in enumerate(line.strip())
        }

    part1(input_image, enhancement_algorithm)
    part2(input_image, enhancement_algorithm)


if __name__ == "__main__":
    main()
