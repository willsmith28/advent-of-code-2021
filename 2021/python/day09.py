import pathlib

with open(pathlib.Path(__file__).parent.parent / "input" / "day09") as input_file:
    input_data = [
        list(map(int, line.strip())) for line in input_file.readlines() if line != "\n"
    ]


def part1():
    """
    --- Day 9: Smoke Basin ---

    These caves seem to be lava tubes. Parts are even still volcanically active; small hydrothermal vents release smoke into the caves that slowly settles like rain.

    If you can model how the smoke flows through the caves, you might be able to avoid it and be that much safer. The submarine generates a heightmap of the floor of the nearby caves for you (your puzzle input).

    Smoke flows to the lowest point of the area it's in. For example, consider the following heightmap:

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678

    Each number corresponds to the height of a particular location, where 9 is the highest and 0 is the lowest a location can be.

    Your first goal is to find the low points - the locations that are lower than any of its adjacent locations. Most locations have four adjacent locations (up, down, left, and right); locations on the edge or corner of the map have three or two adjacent locations, respectively. (Diagonal locations do not count as adjacent.)

    In the above example, there are four low points, all highlighted: two are in the first row (a 1 and a 0), one is in the third row (a 5), and one is in the bottom row (also a 5). All other locations on the heightmap have some lower adjacent location, and so are not low points.

    The risk level of a low point is 1 plus its height. In the above example, the risk levels of the low points are 2, 1, 6, and 6. The sum of the risk levels of all low points in the heightmap is therefore 15.

    Find all of the low points on your heightmap. What is the sum of the risk levels of all low points on your heightmap?
    """
    low_points = {}
    last_row_index = len(input_data) - 1
    last_column_index = len(input_data[0]) - 1
    for row in range(len(input_data)):
        for column in range(len(input_data[0])):
            height = input_data[row][column]
            is_low_point = False
            match (row, column):
                # top left corner
                case (0, 0):
                    is_low_point = (
                        height < input_data[0][1] and height < input_data[1][0]
                    )

                # top right corner
                case (0, j) if j == last_column_index:
                    is_low_point = (
                        height < input_data[1][j] and height < input_data[0][j - 1]
                    )

                # bottom left corner
                case (i, 0) if i == last_row_index:
                    is_low_point = (
                        height < input_data[i - 1][0] and height < input_data[i][1]
                    )

                # bottom right corner
                case (i, j) if j == last_column_index and i == last_row_index:
                    is_low_point = (
                        height < input_data[i - 1][j] and height < input_data[i][j - 1]
                    )

                # right side non corner
                case (i, j) if j == last_column_index:
                    is_low_point = (
                        height < input_data[i + 1][j]
                        and height < input_data[i - 1][j]
                        and height < input_data[i][j - 1]
                    )

                # left side non corner
                case (i, 0):
                    is_low_point = (
                        height < input_data[i + 1][0]
                        and height < input_data[i - 1][0]
                        and height < input_data[i][1]
                    )

                # top row non corners
                case (0, j):
                    is_low_point = (
                        height < input_data[1][j]
                        and height < input_data[0][j + 1]
                        and height < input_data[0][j - 1]
                    )

                # bottom row non corner
                case (i, j) if i == last_row_index:
                    is_low_point = (
                        height < input_data[i - 1][j]
                        and height < input_data[i][j + 1]
                        and height < input_data[i][j - 1]
                    )

                # other points
                case (i, j):
                    is_low_point = (
                        height < input_data[i + 1][j]
                        and height < input_data[i - 1][j]
                        and height < input_data[i][j + 1]
                        and height < input_data[i][j - 1]
                    )

            if is_low_point:
                low_points[(row, column)] = height

    print(sum(val + 1 for val in low_points.values()))
    return list(low_points.keys())


def part2(local_minimums: list[tuple[int, int]]):
    """
    --- Part Two ---

    Next, you need to find the largest basins so you know what areas are most important to avoid.

    A basin is all locations that eventually flow downward to a single low point. Therefore, every low point has a basin, although some basins are very small. Locations of height 9 do not count as being in any basin, and all other locations will always be part of exactly one basin.

    The size of a basin is the number of locations within the basin, including the low point. The example above has four basins.

    The top-left basin, size 3:

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678

    The top-right basin, size 9:

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678

    The middle basin, size 14:

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678

    The bottom-right basin, size 9:

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678

    Find the three largest basins and multiply their sizes together. In the above example, this is 9 * 14 * 9 = 1134.

    What do you get if you multiply together the sizes of the three largest basins?
    """
    basins = [count_basin(x, y, set()) for x, y in local_minimums]
    b1, b2, b3, *_ = sorted(basins, reverse=True)
    print(b1 * b2 * b3)


def count_basin(row: int, column: int, visited: set[tuple[int, int]]):
    point = (row, column)
    try:
        if (
            row == -1
            or column == -1
            or input_data[row][column] == 9
            or point in visited
        ):
            return 0
    except IndexError:
        return 0

    visited.add(point)
    return (
        1
        + count_basin(row, column + 1, visited)
        + count_basin(row, column - 1, visited)
        + count_basin(row + 1, column, visited)
        + count_basin(row - 1, column, visited)
    )


if __name__ == "__main__":
    local_minimums = part1()
    part2(local_minimums)
