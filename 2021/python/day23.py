import enum
import functools
import heapq
import math
import operator
import pathlib
import time
from typing import NamedTuple, TypeAlias


class SpaceType(str, enum.Enum):
    hallway = "hallway"
    hallway_room_adjacent = "hallway_room_adjacent"
    room = "room"


class AmphipodType(str, enum.Enum):
    A = "A"
    B = "B"
    C = "C"
    D = "D"


class Amphipod(NamedTuple):
    type: AmphipodType
    position: tuple[int, int]


class Space(NamedTuple):
    occupant: Amphipod | None
    space_type: SpaceType
    room_number: int | None
    neighbors: tuple[tuple[int, int], ...]
    position: tuple[int, int]


Board: TypeAlias = dict[tuple[int, int], Space]
HashableBoard: TypeAlias = frozenset[tuple[tuple[int, int], Space]]


class PriorityQueue:
    def __init__(self) -> None:
        self.elements: list[tuple[float, int, HashableBoard]] = []
        self.push_id = 0

    @property
    def is_empty(self) -> bool:
        return not self.elements

    def push(self, item: HashableBoard, priority: float):
        heapq.heappush(self.elements, (priority, self.push_id, item))
        self.push_id += 1

    def pop(self) -> HashableBoard:
        return heapq.heappop(self.elements)[2]


TARGET_ROOM = {
    AmphipodType.A.value: 3,
    AmphipodType.B.value: 5,
    AmphipodType.C.value: 7,
    AmphipodType.D.value: 9,
}
ENERGY_PER_MOVE = {
    AmphipodType.A.value: 1,
    AmphipodType.B.value: 10,
    AmphipodType.C.value: 100,
    AmphipodType.D.value: 1000,
}


def print_board(board: Board):
    max_x = max(x for x, _ in board.keys())
    max_y = max(y for _, y in board.keys())
    for y in range(max_y + 2):
        for x in range(max_x + 2):
            position = (x, y)
            if position not in board:
                print("#", end="")
            elif board[position].occupant:
                print(board[position].occupant.type, end="")
            else:
                print(".", end="")

        print("\n", end="")


def hashable_board(board: Board) -> HashableBoard:
    return frozenset(tuple(items) for items in board.items())


def unhash_board(hashed_board: HashableBoard) -> Board:
    return dict(hashed_board)


def parse_input_file(unfold=False) -> Board:
    raw_data: list[list[str]] = []
    with open(pathlib.Path(__file__).parent.parent / "input" / "day23") as input_file:
        for line in input_file.readlines():
            raw_data.append(list(line.rstrip()))

    if unfold:
        raw_data.insert(3, list("  #D#C#B#A#"))
        raw_data.insert(4, list("  #D#B#A#C#"))

    board: dict[tuple(int, int), Space] = {}
    for row_index, row in enumerate(raw_data):
        for column_index, value in enumerate(row):
            if value in (" ", "#"):
                continue

            neighboring_spaces = (
                (column_index, row_index + 1),
                (column_index, row_index - 1),
                (column_index + 1, row_index),
                (column_index - 1, row_index),
            )
            neighbors = []
            for x, y in neighboring_spaces:
                try:
                    if (
                        x < 0
                        or y < 0
                        or raw_data[y][x] == "#"
                        or raw_data[y][x].isspace()
                    ):
                        continue

                    neighbors.append((x, y))
                except IndexError:
                    continue

            position = (column_index, row_index)
            room_number = None
            amphipod = None
            if value in AmphipodType.__members__:
                amphipod = Amphipod(type=value, position=position)
                space_type = SpaceType.room.value
                room_number = column_index

            elif len(neighbors) == 3:
                space_type = SpaceType.hallway_room_adjacent.value

            else:
                space_type = SpaceType.hallway.value

            board[position] = Space(
                occupant=amphipod,
                space_type=space_type,
                neighbors=tuple(neighbors),
                position=position,
                room_number=room_number,
            )

    return board


def room_blocked(
    board: Board,
    starting_position: tuple[int, int],
):
    current_space = board[starting_position]
    while True:
        x, y = current_space.position
        if y == 1:
            return False

        current_space = board[(x, y - 1)]
        if current_space.occupant:
            return True


def hallway_blocked(board: Board, starting_position: tuple[int, int], target: int):
    distance = target - starting_position[0]
    step = int(math.copysign(1, distance))
    current_space = board[starting_position]
    for _ in range(abs(distance)):
        x, y = current_space.position
        current_space = board[(x + step, y)]
        if current_space.occupant:
            return True

    return False


def room_empty_or_has_correct_amphipods(
    x: int, board: Board, amphipod_type: AmphipodType
):
    row_length = max(y for _, y in board.keys()) + 1
    return all(
        (space := board[(x, y)]).occupant is None
        or space.occupant.type == amphipod_type
        for y in range(2, row_length)
    )


@functools.cache
def amphipod_possible_movements(
    amphipod: Amphipod, board_state: HashableBoard
) -> list[tuple[tuple[int, int], int]]:
    board = unhash_board(board_state)
    current_space = board[amphipod.position]
    possible_movements = [(current_space.position, 0)]
    # am I in the correct room with the correct amphipods?
    if current_space.room_number == TARGET_ROOM[
        amphipod.type
    ] and room_empty_or_has_correct_amphipods(
        current_space.position[0], board, amphipod.type
    ):
        return possible_movements

    # if I'm in a room where can I go in the hallway
    if current_space.space_type == SpaceType.room:
        # am I blocked towards the hallway
        if room_blocked(board, current_space.position):
            return possible_movements

        # find hallway adjacent space
        move_count_at_hallway = current_space.position[1] - 1
        current_space = board[(current_space.position[0], 1)]

        x, y = current_space.position
        # look left for possible positions
        stack = [(board[(x - 1, y)], move_count_at_hallway + 1)]
        while stack:
            current_space, move_count = stack.pop()
            if current_space.occupant:
                break

            if current_space.space_type == SpaceType.hallway:
                possible_movements.append((current_space.position, move_count))

            if len(current_space.neighbors) > 1:
                stack.append(
                    (
                        board[
                            (current_space.position[0] - 1, current_space.position[1])
                        ],
                        move_count + 1,
                    )
                )

        # look right for possible positions
        stack = [(board[(x + 1, y)], move_count_at_hallway + 1)]
        while stack:
            current_space, move_count = stack.pop()
            if current_space.occupant:
                break

            if current_space.space_type == SpaceType.hallway:
                possible_movements.append((current_space.position, move_count))

            if len(current_space.neighbors) > 1:
                stack.append(
                    (
                        board[
                            (current_space.position[0] + 1, current_space.position[1])
                        ],
                        move_count + 1,
                    )
                )

        return possible_movements

    # if I'm in the hallway can I go to my room
    target_room_column = TARGET_ROOM[amphipod.type]
    if room_empty_or_has_correct_amphipods(
        target_room_column, board, amphipod.type
    ) and not hallway_blocked(board, current_space.position, target_room_column):
        target_room_row = 2
        next_room = board[(target_room_column, target_room_row + 1)]
        while next_room.occupant is None:
            target_room_row = next_room.position[1]
            try:
                next_room = board[(target_room_column, target_room_row + 1)]
            except KeyError:
                break

    else:
        return possible_movements

    x_distance = target_room_column - current_space.position[0]
    step = math.copysign(1, x_distance)
    x_distance = abs(x_distance)
    # move towards room
    move_count = 0
    for _ in range(x_distance):
        x, y = current_space.position
        current_space = board[(x + step, y)]
        move_count += 1
        if current_space.occupant:
            return possible_movements

    y_distance = abs(target_room_row - current_space.position[1])
    possible_movements.append(
        ((target_room_column, target_room_row), x_distance + y_distance)
    )
    return possible_movements


@functools.cache
def next_possible_moves(
    starting_state: HashableBoard,
) -> list[tuple[HashableBoard, int]]:
    board = unhash_board(starting_state)
    amphipods = (
        space.occupant for space in board.values() if space.occupant is not None
    )
    no_movement = (starting_state, 0)
    all_possible_boards = {no_movement}
    for amphipod in amphipods:
        possible_moves = amphipod_possible_movements(amphipod, starting_state)
        if len(possible_moves) == 1:
            continue

        for new_posiiton, moves in possible_moves:
            old_from_space = board[amphipod.position]
            old_to_space = board[new_posiiton]
            added_cost = ENERGY_PER_MOVE[amphipod.type] * moves
            new_board = {
                **board,
                old_from_space.position: Space(
                    occupant=None,
                    space_type=old_from_space.space_type,
                    room_number=old_from_space.room_number,
                    neighbors=old_from_space.neighbors,
                    position=old_from_space.position,
                ),
                old_to_space.position: Space(
                    occupant=Amphipod(
                        type=amphipod.type,
                        position=old_to_space.position,
                    ),
                    space_type=old_to_space.space_type,
                    room_number=old_to_space.room_number,
                    neighbors=old_to_space.neighbors,
                    position=old_to_space.position,
                ),
            }

            all_possible_boards.add((hashable_board(new_board), added_cost))

    all_possible_boards.discard(no_movement)
    return sorted(all_possible_boards, key=operator.itemgetter(1))


def dijkstra(starting_state: HashableBoard, goal_state: HashableBoard):
    frontier = PriorityQueue()
    frontier.push(starting_state, 0)
    cost_to_state = {starting_state: 0}

    while not frontier.is_empty:
        current_state = frontier.pop()

        if current_state == goal_state:
            break

        for next_state, movement_cost in next_possible_moves(current_state):
            next_cost = cost_to_state[current_state] + movement_cost
            if next_state not in cost_to_state or next_cost < cost_to_state[next_state]:
                cost_to_state[next_state] = next_cost
                frontier.push(next_state, next_cost)

    return cost_to_state


def find_fewest_moves(unfold_input=False):
    board = parse_input_file(unfold=unfold_input)
    amphipod_room_mapping = {val: key for key, val in TARGET_ROOM.items()}
    goal_board = {
        key: (
            Space(
                occupant=Amphipod(
                    type=amphipod_room_mapping[val.room_number], position=key
                ),
                space_type=val.space_type,
                room_number=val.room_number,
                neighbors=val.neighbors,
                position=val.position,
            )
            if val.space_type == SpaceType.room
            else val
        )
        for key, val in board.items()
    }
    goal_state = hashable_board(goal_board)
    starting_state = hashable_board(board)
    start = time.time()
    result = dijkstra(starting_state, goal_state)
    end = time.time()
    print(result[goal_state])
    print(f"{end - start} seconds elapsed")


def main():
    find_fewest_moves()
    find_fewest_moves(unfold_input=True)


if __name__ == "__main__":
    main()
